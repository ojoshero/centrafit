//
//  LogInViewController.swift
//  CentraFit
//
//  Created by Joshua Martin on 18/02/2015.
//  Copyright (c) 2015 Joshua Martin. All rights reserved.
//

import UIKit
import CoreData

let ref = Firebase(url: "https://blinding-heat-2824.firebaseio.com")

class LogInViewController: UIViewController, UIGestureRecognizerDelegate, UITextFieldDelegate {
    
    @IBOutlet var swipeDown: UISwipeGestureRecognizer!
    @IBOutlet var tap: UITapGestureRecognizer!
    
    @IBAction func onSwipeDown(sender: UISwipeGestureRecognizer) {
        self.email.resignFirstResponder()
        self.password.resignFirstResponder()
    }
    
    @IBAction func onTap(sender: UITapGestureRecognizer) {
        self.email.resignFirstResponder()
        self.password.resignFirstResponder()
    }
    
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var pageLabel: UILabel!
    
    @IBAction func checkEmailEdited(sender: UITextField) {
        if self.pageLabel.text == "Invalid email entered!" {
            self.pageLabel.text = "Register or login below:"
            self.pageLabel.textColor = UIColor.whiteColor()
        }
    }
    
    @IBAction func checkPasswordEdited(sender: UITextField) {
        if self.pageLabel.text == "Invalid password entered!" {
            self.pageLabel.text = "Register or login below:"
            self.pageLabel.textColor = UIColor.whiteColor()
        }
    }
    
    @IBAction func registerUser(sender: UIButton) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        dispatch_async(dispatch_get_main_queue(), {
        ref.createUser(self.email.text, password: self.password.text,
            withValueCompletionBlock: { error, result in
                if error != nil {
                    // There was an error creating the account
                    println(error)
                    if let errorCode = FAuthenticationError(rawValue: error.code) {
                        switch (errorCode) {
                        case .InvalidEmail:
                            self.pageLabel.text = "Invalid email entered!"
                            self.pageLabel.textColor = UIColor.redColor()
                            break
                        case .EmailTaken:
                            self.pageLabel.text = "Email already taken!"
                            self.pageLabel.textColor = UIColor.redColor()
                            break
                        case .InvalidPassword:
                            self.pageLabel.text = "Invalid password entered!"
                            self.pageLabel.textColor = UIColor.redColor()
                            break
                        default:
                            break
                        }
                    }
                } else {
                    let uid = result["uid"] as? String
                    println("Successfully created user account with uid: \(uid)")
                    dispatch_async(dispatch_get_main_queue(), {
                        ref.authUser(self.email.text, password: self.password.text) {
                            error, authData in
                            if error != nil {
                                println(error)
                            } else {
                                // Authentication just completed successfully :)
                                let newUser = [
                                    "provider": authData.provider,
                                    "email": authData.providerData["email"] as? NSString as? String,
                                    "username": authData.providerData["email"] as? NSString as? String,
                                    "goals": "",
                                    "plans": "0"
                                ]
                                ref.childByAppendingPath("users")
                                    .childByAppendingPath(authData.uid).setValue(newUser)
                                
                                var fetchRequest = NSFetchRequest(entityName: "User")
                                if let fetchResults = managedObjectContext!.executeFetchRequest(fetchRequest, error: nil) as? [User] {
                                    let users = fetchResults
                                    for user in users {
                                        user.loggedIn = false
                                    }
                                }
                                
                                var userDetails = NSEntityDescription.insertNewObjectForEntityForName("User", inManagedObjectContext: managedObjectContext!) as! User
                                userDetails.id = authData.uid
                                userDetails.email = self.email.text
                                userDetails.loggedIn = true
                                userDetails.username = self.email.text
                                userDetails.goals = ""
                                userDetails.plans = 0
                                managedObjectContext?.save(nil)
                                
                                self.performSegueWithIdentifier("GoToMain", sender: self)
                            }
                        }
                    })
                }
            })
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
        })
    }
    
    @IBAction func login(sender: UIButton) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        dispatch_async(dispatch_get_main_queue(), {
            ref.authUser(self.email.text, password: self.password.text) {
                error, authData in
                if error != nil {
                    // an error occured while attempting login
                    if let errorCode = FAuthenticationError(rawValue: error.code) {
                        switch (errorCode) {
                        case .InvalidEmail:
                            self.pageLabel.text = "Invalid email entered!"
                            self.pageLabel.textColor = UIColor.redColor()
                            break
                        case .InvalidPassword:
                            self.pageLabel.text = "Invalid password entered!"
                            self.pageLabel.textColor = UIColor.redColor()
                            break
                        case .UserDoesNotExist:
                            self.pageLabel.text = "Email address not recognised!"
                            self.pageLabel.textColor = UIColor.redColor()
                            break
                        default:
                            break
                        }
                    }
                    
                } else {
                    // user is logged in, check authData for data
                    var userFound = false
                    var fetchRequest = NSFetchRequest(entityName: "User")
                    if let fetchResults = managedObjectContext!.executeFetchRequest(fetchRequest, error: nil) as? [User] {
                        let users = fetchResults
                        for user in users {
                            user.loggedIn = false
                            if authData.uid == user.id {
                                userFound = true
                                user.loggedIn = true
                                managedObjectContext?.save(nil)
                                self.performSegueWithIdentifier("GoToMain", sender: self)
                            }
                        }
                    }
                    if userFound == false {
                        dispatch_async(dispatch_get_main_queue(), {
                            ref.childByAppendingPath("users")
                                .childByAppendingPath(authData.uid).observeSingleEventOfType(.Value, withBlock: { snapshot in
                                    
                                    var userDetails = NSEntityDescription.insertNewObjectForEntityForName("User", inManagedObjectContext: managedObjectContext!) as! User
                                    userDetails.id = authData.uid
                                    userDetails.email = self.email.text
                                    userDetails.loggedIn = true
                                    if let goals = snapshot.value["goals"] as? String {
                                        userDetails.goals = goals
                                    }
                                    if let plans = snapshot.value["plans"] as? Int {
                                        userDetails.plans = plans
                                    }
                                    if let username = snapshot.value["username"] as? String {
                                        userDetails.username = username
                                    }
                                    userDetails.password = self.password.text
                                    managedObjectContext?.save(nil)
                                })
                            self.performSegueWithIdentifier("GoToMain", sender: self)
                        })
                    }
                }
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            }
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addGestureRecognizer(self.tap)
        self.view.addGestureRecognizer(self.swipeDown)
        
        self.email.delegate = self
        self.password.delegate = self
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        if let context = appDelegate.managedObjectContext {
            managedObjectContext = context
        }
        
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "background-2.png")!)
    }
    
    override func viewDidAppear(animated: Bool) {
        
        var fetchRequest = NSFetchRequest(entityName: "User")
        if let fetchResults = managedObjectContext!.executeFetchRequest(fetchRequest, error: nil) as? [User] {
            let users = fetchResults
            for user in users {
                if user.loggedIn == true {
                    UIApplication.sharedApplication().networkActivityIndicatorVisible = true
                    dispatch_async(dispatch_get_main_queue(), {
                        ref.authUser(user.email, password: user.password) {
                            error, authData in
                            if error != nil {
                                // an error occured while attempting login
                                println(error)
                            } else {
                                // user is logged in, check authData for data
                                var fetchRequest = NSFetchRequest(entityName: "User")
                                if let fetchResults = managedObjectContext!.executeFetchRequest(fetchRequest, error: nil) as? [User] {
                                    let users = fetchResults
                                    for user in users {
                                        user.loggedIn = false
                                        if authData.uid == user.id {
                                            user.loggedIn = true
                                            managedObjectContext?.save(nil)
                                        }
                                    }
                                }
                            }
                        }
                        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                    })
                    self.performSegueWithIdentifier("GoToMain", sender: self)
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation
/*
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        println("whats")
    }
*/
}
