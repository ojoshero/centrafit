//
//  SettingsViewController.swift
//  CentraFit
//
//  Created by Joshua Martin on 19/02/2015.
//  Copyright (c) 2015 Joshua Martin. All rights reserved.
//

import UIKit
import CoreData

var goalList = ["Weight Loss", "Build Muscle Size", "Endurance", "Increase Strength", "Abs", "Muscle Tone"]
var currentGoal = ""

class SettingsViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, UIGestureRecognizerDelegate, UITextFieldDelegate, UITextViewDelegate {
    
    @IBOutlet var tapGestureRecogniser: UITapGestureRecognizer!
    @IBOutlet var swipeGestureRecogniser: UISwipeGestureRecognizer!
    
    @IBOutlet weak var goBack: UIButton!
    @IBOutlet weak var logoutButton: UIButton!
    
    @IBOutlet weak var pickerContainer: UIView!
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var goals: UITextView!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var usernameLabel: UILabel!
    
    var usernameText:String?
    var goalsText:String?
    
    @IBAction func onTap(sender: UITapGestureRecognizer) {
        self.username.resignFirstResponder()
        self.pickerContainer.hidden = true
    }
    
    @IBAction func onSwipeDown(sender: UISwipeGestureRecognizer) {
        self.username.resignFirstResponder()
        self.pickerContainer.hidden = true
    }
    
    @IBAction func showGoalPicker(sender: AnyObject) {
        self.pickerContainer.hidden = false
    }
    
    @IBAction func closeGoals(sender: UIButton) {
        self.pickerContainer.hidden = true
    }
    
    @IBAction func addGoal(sender: UIButton) {
        if (self.goals.text == "") {
            self.goals.text = currentGoal
        } else {
            self.goals.text = self.goals.text + ", " + currentGoal
        }
    }
    
    @IBAction func clearGoals(sender: UIButton) {
        self.goals.text = ""
    }
    
    @IBAction func enteredUsername(sender: UITextField) {
        if self.usernameLabel.text == "This username is taken" {
            self.usernameLabel.text = "Set to email as default."
            self.usernameLabel.textColor = UIColor.whiteColor()
        }
    }
    
    @IBAction func back(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func confirmChanges(sender: UIButton) {
        var fetchRequest = NSFetchRequest(entityName: "User")
        if let fetchResults = managedObjectContext!.executeFetchRequest(fetchRequest, error: nil) as? [User] {
            let users = fetchResults
            for user in users {
                if user.loggedIn == true {
                    var newUsername = self.username.text
                    var newGoals = self.goals.text
                    // Check if username has changed since coming into the page.
                    if usernameText != newUsername {
                        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
                        dispatch_async(dispatch_get_main_queue(), {
                            let ref2 = Firebase(url: "https://blinding-heat-2824.firebaseio.com/users")
                            ref2.observeSingleEventOfType(.Value, withBlock: { snapshot in
                                var children = snapshot.children
                                while let child = children.nextObject() as? FDataSnapshot {
                                    if user.id != child.key && newUsername == child.value["username"] as? String {
                                        self.usernameLabel.textColor = UIColor.redColor()
                                        self.usernameLabel.text = "This username is taken"
                                        return
                                    }
                                    ref2.childByAppendingPath(user.id).childByAppendingPath("username").setValue(newUsername)
                                    user.username = newUsername
                                    managedObjectContext?.save(nil)
                                }
                            })
                            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                        })
                    }
                    // Check if goals have changed since entering the page.
                    if goalsText != newGoals {
                        user.goals = newGoals
                        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
                        dispatch_async(dispatch_get_main_queue(), {
                            let ref2 = Firebase(url: "https://blinding-heat-2824.firebaseio.com/users")
                            ref2.childByAppendingPath(user.id).childByAppendingPath("goals").setValue(newGoals)
                            user.goals = newGoals
                            managedObjectContext?.save(nil)
                            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                        })
                    }
                }
            }
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    @IBAction func logout(sender: UIButton) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        dispatch_async(dispatch_get_main_queue(), {
            ref.unauth()
            var fetchRequest = NSFetchRequest(entityName: "User")
            if let fetchResults = managedObjectContext!.executeFetchRequest(fetchRequest, error: nil) as? [User] {
                let users = fetchResults
                for user in users {
                    user.loggedIn = false
                }
            }
            managedObjectContext?.save(nil)
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            
            self.performSegueWithIdentifier("Back to Login", sender: self)
        })
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return goalList.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        return goalList[row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        currentGoal = goalList[row]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addGestureRecognizer(self.tapGestureRecogniser)
        self.view.addGestureRecognizer(self.swipeGestureRecogniser)
        
        self.username.delegate = self
        self.goals.delegate = self
        
        if let a = usernameText {
            self.username.text = a
            if let b = goalsText {
                self.goals.text = b
            }
        }
        
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "background-2.png")!)
        
        self.pickerView.dataSource = self
        self.pickerView.delegate = self

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
