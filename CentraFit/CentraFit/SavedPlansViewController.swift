//
//  SavedPlansViewController.swift
//  CentraFit
//
//  Created by Joshua Martin on 23/01/2015.
//  Copyright (c) 2015 Joshua Martin. All rights reserved.
//

import UIKit
import CoreData

class SavedPlansViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var items = [Program]()
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var dataTable: UITableView!
    
    @IBAction func goBack(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var fetchRequest = NSFetchRequest(entityName: "Program")
        if let fetchResults = managedObjectContext!.executeFetchRequest(fetchRequest, error: nil) as? [Program] {
            items = fetchResults
        }
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "background-2.png")!)
        
        self.dataTable.delegate = self
        self.dataTable.dataSource = self
        self.dataTable.reloadData()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:SavedPlansTableCell = tableView.dequeueReusableCellWithIdentifier("reuseCell", forIndexPath: indexPath) as! SavedPlansTableCell
        
        cell.title.text = items[indexPath.row].title
        cell.category.text = items[indexPath.row].category
        var fetchRequest = NSFetchRequest(entityName: "Steps")
        var st = [Steps]()
        var equipment = ""
        if let fetchResults = managedObjectContext!.executeFetchRequest(fetchRequest, error: nil) as? [Steps] {
            let steps = fetchResults
            for step in steps {
                if step.id == items[indexPath.row].id {
                    st.append(step)
                    equipment += step.equipment + ", "
                    cell.planImage.image = UIImage(data: step.image)
                }
            }
        }
        cell.numberOfSteps.text = String(st.count) + " steps"
        cell.equipmentRequired.text = equipment
        cell.infoButton.tag = indexPath.row
        
        cell.planImage.layer.cornerRadius = 12.5
        cell.planImage.layer.masksToBounds = true
        cell.planImage.layer.borderColor = UIColor.whiteColor().CGColor
        cell.planImage.layer.borderWidth = 2.0
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.dataTable.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    // Override to support editing the table view.
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            println(self.items[indexPath.row].id)
            
            // Delete the row from the data source
            var fetchRequest = NSFetchRequest(entityName: "Steps")
            if let fetchResults = managedObjectContext!.executeFetchRequest(fetchRequest, error: nil) as? [Steps] {
                let res = fetchResults
                for step in res {
                    if step.id == self.items[indexPath.row].id {
                        managedObjectContext?.deleteObject(step as NSManagedObject)
                    }
                }
            }
            managedObjectContext?.deleteObject(self.items[indexPath.row] as NSManagedObject)
            self.items.removeAtIndex(indexPath.row)
            managedObjectContext?.save(nil)
            
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "displayWorkout" {
            let destination = segue.destinationViewController as! DisplayViewController
            let indexPath = self.dataTable?.indexPathForSelectedRow()
            println(indexPath?.row)
            if let row:Int = indexPath?.row {
                destination.planTitleText = items[row].title
                destination.categoryText = items[row].category
        
                var fetchRequest = NSFetchRequest(entityName: "Steps")
                if let fetchResults = managedObjectContext!.executeFetchRequest(fetchRequest, error: nil) as? [Steps] {
                    let res = fetchResults
                    for step in res {
                        if step.id == items[row].id {
                            var stepData = displaySteps(name: "", equipment: "", reps: "", time: "", sets: "", image: NSData(), number: NSNumber(), details: "")
                            stepData.name = step.name
                            stepData.equipment = step.equipment
                            stepData.reps = step.reps
                            stepData.time = step.time
                            stepData.sets = step.sets
                            stepData.image = step.image
                            stepData.number = step.number
                            stepData.details = step.details
                            destination.stepStore.append(stepData)
                        }
                    }
                }
            }
        } else if segue.identifier == "Show Saved Detail" {
            let destination = segue.destinationViewController as! PlanDetailsViewController
            let indexPath = self.dataTable?.indexPathForSelectedRow()
            if let row:Int = sender?.tag {
                destination.planTitleText = items[row].title
                var fetchRequest = NSFetchRequest(entityName: "User")
                if let fetchResults = managedObjectContext!.executeFetchRequest(fetchRequest, error: nil) as? [User] {
                    let users = fetchResults
                    for user in users {
                        if user.id == items[row].creatorID {
                            if user.loggedIn == true {
                                destination.nameText = "You"
                                destination.buttonHidden = false
                                var fetchRequest2 = NSFetchRequest(entityName: "Steps")
                                if let fetchResults2 = managedObjectContext!.executeFetchRequest(fetchRequest2, error: nil) as? [Steps] {
                                    let res = fetchResults2
                                    for step in res {
                                        if step.id == items[row].id {
                                            var stepData = displaySteps(name: "", equipment: "", reps: "", time: "", sets: "", image: NSData(), number: NSNumber(), details: "")
                                            stepData.name = step.name
                                            stepData.equipment = step.equipment
                                            stepData.reps = step.reps
                                            stepData.time = step.time
                                            stepData.sets = step.sets
                                            stepData.image = step.image
                                            stepData.number = step.number
                                            stepData.details = step.details
                                            destination.stepStore.append(stepData)
                                        }
                                    }
                                }
                                var fetchRequest3 = NSFetchRequest(entityName: "Program")
                                if let fetchResults3 = managedObjectContext!.executeFetchRequest(fetchRequest3, error: nil) as? [Program] {
                                    let res = fetchResults3
                                    for program in res {
                                        if program.id == items[row].id {
                                            var programData = programStruct(category: "", creatorID: "", id: "", title: "", rating: 0, downloads: 0)
                                            programData.category = program.category
                                            programData.creatorID = items[row].creatorID
                                            programData.id = program.id
                                            programData.title = program.title
                                            programData.rating = Int(program.rating)
                                            programData.downloads = Int(program.downloads)
                                            destination.items.append(programData)
                                        }
                                    }
                                }
                                return
                            } else {
                                destination.nameText = user.username
                                destination.buttonHidden = true
                            }
                        }
                    }
                }
            }
        }
    }
}

