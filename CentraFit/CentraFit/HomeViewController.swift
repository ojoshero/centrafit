//
//  HomeViewController.swift
//  CentraFit
//
//  Created by Joshua Martin on 19/01/2015.
//  Copyright (c) 2015 Joshua Martin. All rights reserved.
//

import UIKit
import CoreData

var savedPlans = []

var managedObjectContext:NSManagedObjectContext?

class HomeViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        if let context = appDelegate.managedObjectContext {
            managedObjectContext = context
        }
        
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "background-2.png")!)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "Show Settings" {
            let destination = segue.destinationViewController as! SettingsViewController
            var fetchRequest = NSFetchRequest(entityName: "User")
            if let fetchResults = managedObjectContext!.executeFetchRequest(fetchRequest, error: nil) as? [User] {
                let users = fetchResults
                for user in users {
                    if user.loggedIn == true {
                        destination.usernameText = user.username
                        destination.goalsText = user.goals
                    }
                }
            }
        }
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }

}
