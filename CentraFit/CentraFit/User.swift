//
//  User.swift
//  CentraFit
//
//  Created by Joshua Martin on 18/02/2015.
//  Copyright (c) 2015 Joshua Martin. All rights reserved.
//

import Foundation
import CoreData

@objc(User)
class User: NSManagedObject {

    @NSManaged var id: String
    @NSManaged var loggedIn: NSNumber
    @NSManaged var username: String
    @NSManaged var password: String
    @NSManaged var email: String
    @NSManaged var goals: String
    @NSManaged var plans: NSNumber
    @NSManaged var newRelationship: Program
    @NSManaged var newRelationship1: Steps
    
}
