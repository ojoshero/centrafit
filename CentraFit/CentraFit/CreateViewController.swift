//
//  CreateViewController.swift
//  CentraFit
//
//  Created by Joshua Martin on 20/01/2015.
//  Copyright (c) 2015 Joshua Martin. All rights reserved.
//

import UIKit
import CoreData
import AVFoundation

struct steps {
    var name:String
    var equipment:String
    var reps:String
    var time:String
    var sets:String
    var image:NSData
    var details:String
}

class CreateViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UIGestureRecognizerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextViewDelegate, UITextFieldDelegate {
    
    var stepStore = [steps]()
    var images = [NSData]()
    
    var scrollViewConstant:CGFloat = 0.0
    
    // Gesture outlets
    @IBOutlet var tap: UITapGestureRecognizer!
    @IBOutlet var swipeRight: UISwipeGestureRecognizer!
    @IBOutlet var swipeLeft: UISwipeGestureRecognizer!
    @IBOutlet var swipeDown: UISwipeGestureRecognizer!
    
    // Page outlets
    @IBOutlet weak var planTitle: UITextField!
    @IBOutlet weak var planCategory: UITextField!
    @IBOutlet weak var stepNumbers: UILabel!
    @IBOutlet weak var stepTitle: UITextField!
    @IBOutlet weak var equipment: UITextField!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var addStep: UIButton!
    @IBOutlet weak var deleteStep: UIButton!
    @IBOutlet weak var stepView: UIScrollView!
    @IBOutlet weak var complete: UIButton!
    @IBOutlet weak var reps: UITextField!
    @IBOutlet weak var time: UITextField!
    @IBOutlet weak var sets: UITextField!
    @IBOutlet weak var addPhoto: UIButton!
    @IBOutlet weak var detailsTextView: UITextView!
    @IBOutlet weak var scrollViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var photoLabel: UIButton!
    @IBOutlet weak var displayPhoto: UIImageView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var addCategory: UIButton!
    @IBOutlet weak var donePickerView: UIButton!
    @IBOutlet weak var pickerView: UIPickerView!
    
    // Gesture functions
    @IBAction func onTap(sender: UITapGestureRecognizer) {
        self.planTitle.resignFirstResponder()
        self.planCategory.resignFirstResponder()
        self.stepTitle.resignFirstResponder()
        self.equipment.resignFirstResponder()
        self.reps.resignFirstResponder()
        self.time.resignFirstResponder()
        self.sets.resignFirstResponder()
        self.stepView.resignFirstResponder()
        self.detailsTextView.resignFirstResponder()
    }
    
    @IBAction func onSwipeDown(sender: UISwipeGestureRecognizer) {
        self.planTitle.resignFirstResponder()
        self.planCategory.resignFirstResponder()
        self.stepTitle.resignFirstResponder()
        self.equipment.resignFirstResponder()
        self.reps.resignFirstResponder()
        self.time.resignFirstResponder()
        self.sets.resignFirstResponder()
        self.stepView.resignFirstResponder()
        self.detailsTextView.resignFirstResponder()
    }
    
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    @IBAction func clearCategories(sender: UIButton) {
        self.planCategory.text = ""
    }
    
    @IBAction func swipeRight(sender: UISwipeGestureRecognizer) {
        if (self.pageControl.currentPage != 0) {
            var stepData = steps(name: "", equipment: "", reps: "", time: "", sets: "", image: NSData(), details: "")
            stepData.name = stepTitle.text
            stepData.equipment = equipment.text
            stepData.reps = reps.text
            stepData.time = time.text
            stepData.sets = sets.text
            if images.count > self.pageControl.currentPage {
                stepData.image = images[self.pageControl.currentPage]
            } else {
                stepData.image = NSData()
            }
            stepData.details = detailsTextView.text
            stepStore[self.pageControl.currentPage] = stepData
            
            self.setStepBoxDefaults()
            
            let transitionOptions = UIViewAnimationOptions.TransitionFlipFromRight
            
            UIView.transitionWithView(self.stepView, duration: 0.8, options: transitionOptions, animations: {
                
                }, completion: { finished in
                    self.pageControl.currentPage--
                    self.setStepNumbers()
                    self.stepTitle.text = self.stepStore[self.pageControl.currentPage].name
                    self.equipment.text = self.stepStore[self.pageControl.currentPage].equipment
                    self.reps.text = self.stepStore[self.pageControl.currentPage].reps
                    self.sets.text = self.stepStore[self.pageControl.currentPage].sets
                    self.time.text = self.stepStore[self.pageControl.currentPage].time
                    self.detailsTextView.text = self.stepStore[self.pageControl.currentPage].details
            })
        }
    }
    
    @IBAction func swipeLeft(sender: UISwipeGestureRecognizer) {
        if (self.pageControl.currentPage + 1 != self.pageControl.numberOfPages) {
            var stepData = steps(name: "", equipment: "", reps: "", time: "", sets: "", image: NSData(), details: "")
            stepData.name = stepTitle.text
            stepData.equipment = equipment.text
            stepData.reps = reps.text
            stepData.time = time.text
            stepData.sets = sets.text
            if images.count > self.pageControl.currentPage {
                stepData.image = images[self.pageControl.currentPage]
            } else {
                stepData.image = NSData()
            }
            stepData.details = detailsTextView.text
            stepStore[self.pageControl.currentPage] = stepData
            
            self.setStepBoxDefaults()
            
            let transitionOptions = UIViewAnimationOptions.TransitionFlipFromLeft
            
            UIView.transitionWithView(self.stepView, duration: 0.8, options: transitionOptions, animations: {
                
                }, completion: { finished in
                    self.pageControl.currentPage++
                    self.setStepNumbers()
                    if (self.stepStore[self.pageControl.currentPage].name.isEmpty) {
                        self.stepTitle.text = ""
                    } else {
                        self.stepTitle.text = self.stepStore[self.pageControl.currentPage].name
                    }
                    if (self.stepStore[self.pageControl.currentPage].equipment.isEmpty) {
                        self.equipment.text = ""
                    } else {
                        self.equipment.text = self.stepStore[self.pageControl.currentPage].equipment
                    }
                    if (self.stepStore[self.pageControl.currentPage].reps.isEmpty) {
                        self.reps.text = ""
                    } else {
                        self.reps.text = self.stepStore[self.pageControl.currentPage].reps
                    }
                    if (self.stepStore[self.pageControl.currentPage].sets.isEmpty) {
                        self.sets.text = ""
                    } else {
                        self.sets.text = self.stepStore[self.pageControl.currentPage].sets
                    }
                    if (self.stepStore[self.pageControl.currentPage].time.isEmpty) {
                        self.time.text = ""
                    } else {
                        self.time.text = self.stepStore[self.pageControl.currentPage].time
                    }
                    if (self.stepStore[self.pageControl.currentPage].details.isEmpty) {
                        self.detailsTextView.text = ""
                    } else {
                        self.detailsTextView.text = self.stepStore[self.pageControl.currentPage].details
                    }
            })
        }
        
    }
    
    @IBAction func showGoals(sender: UIButton) {
        self.containerView.hidden = false
    }
    
    // Page functions
    @IBAction func goBack(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    @IBAction func addNewStep(sender: UIButton) {
        self.pageControl.numberOfPages++
        self.setStepNumbers()
        self.images.insert(NSData(), atIndex: self.pageControl.currentPage + 1)
        var stepData = steps(name: "", equipment: "", reps: "", time: "", sets: "", image: NSData(), details: "")
        self.stepStore.insert(stepData, atIndex: self.pageControl.currentPage + 1)
    }
    
    @IBAction func deleteThisStep(sender: UIButton) {
        if (self.pageControl.numberOfPages > 1) {
            self.stepStore.removeAtIndex(self.pageControl.currentPage)
            if images.count > self.pageControl.currentPage {
                self.images.removeAtIndex(self.pageControl.currentPage)
            }
            self.pageControl.numberOfPages--
            self.stepTitle.text = self.stepStore[self.pageControl.currentPage].name
            self.equipment.text = self.stepStore[self.pageControl.currentPage].equipment
            self.reps.text = self.stepStore[self.pageControl.currentPage].reps
            self.time.text = self.stepStore[self.pageControl.currentPage].time
            self.sets.text = self.stepStore[self.pageControl.currentPage].sets
            self.detailsTextView.text = self.stepStore[self.pageControl.currentPage].details
            self.setStepNumbers()
        }
    }
    
    func setStepNumbers() {
        self.stepNumbers.text = "Step " + String(self.pageControl.currentPage + 1) + " of " + String(self.pageControl.numberOfPages)
        println(images.count)
        println(self.pageControl.currentPage)
        if images.count > self.pageControl.currentPage {
            if images[self.pageControl.currentPage].length == 0 {
                self.photoLabel.enabled = false
                self.photoLabel.setTitle("No image added for this step.", forState: UIControlState.Normal)
            } else {
                self.photoLabel.enabled = true
                self.photoLabel.setTitle("Image added. Click to view.", forState: UIControlState.Normal)
            }
        } else {
            self.photoLabel.enabled = false
            self.photoLabel.setTitle("No image added for this step.", forState: UIControlState.Normal)
        }
    }
    
    func setStepBoxDefaults() {
        self.stepTitle.text = ""
        self.equipment.text = ""
        self.reps.text = ""
        self.time.text = ""
        self.sets.text = ""
        self.detailsTextView.text = ""
    }
    
    @IBAction func getPhoto(sender: UIButton) {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        let devices = AVCaptureDevice.devices()
        var camera = false
        for device in devices {
            if (device.hasMediaType(AVMediaTypeVideo)) {
                camera = true
            }
        }
        if camera == true {
            actionSheet.addAction(UIAlertAction(title: "Camera", style: UIAlertActionStyle.Default, handler: {
                (value:UIAlertAction!) in
                self.getFromCamera()
            }))
        }
        
        actionSheet.addAction(UIAlertAction(title: "Photos", style: UIAlertActionStyle.Default, handler: {
            (value:UIAlertAction!) in
            self.getFromPhotos()
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(actionSheet, animated: true, completion: nil)
    }
    
    func getFromPhotos() {
        let picker = UIImagePickerController()
        picker.delegate = self
        self.presentViewController(picker, animated: true, completion: nil)
    }
    
    func getFromCamera() {
        let cameraPicker = UIImagePickerController()
        cameraPicker.delegate = self
        cameraPicker.sourceType = UIImagePickerControllerSourceType.Camera
        self.presentViewController(cameraPicker, animated: true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]) {
        self.dismissViewControllerAnimated(true, completion: nil)
        var newImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        var image:NSData = UIImageJPEGRepresentation(newImage, 1.0)
        if images.count < self.pageControl.currentPage + 1 {
            images.insert(image, atIndex: self.pageControl.currentPage)
        } else if images[self.pageControl.currentPage].length == 0 {
            images.insert(image, atIndex: self.pageControl.currentPage)
        } else {
            images[self.pageControl.currentPage] = image
        }
        self.photoLabel.enabled = true
        self.photoLabel.setTitle("Image added. Click to view.", forState: UIControlState.Normal)
    }
    
    @IBAction func displayPhoto(sender: UIButton) {
    }
    
    // These functions mean that the scrollview cannot scroll when inside a textview/textfield. This allows the swipe down gesture to close the keyboard.
    func textViewDidBeginEditing(textView: UITextView) {
        self.stepView.scrollEnabled = false
        //self.scrollViewConstraint.constant = 260
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        self.stepView.scrollEnabled = true
        //self.scrollViewConstraint.constant = self.scrollViewConstant
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        self.stepView.scrollEnabled = false
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        self.stepView.scrollEnabled = true
    }
    
    @IBAction func addCategory(sender: UIButton) {
        if (self.planCategory.text == "") {
            self.planCategory.text = currentGoal
        } else {
            self.planCategory.text = self.planCategory.text + ", " + currentGoal
        }
    }
    
    @IBAction func closePicker(sender: UIButton) {
        self.containerView.hidden = true
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return goalList.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        return goalList[row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        currentGoal = goalList[row]
    }

    // Built in functions
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var stepData = steps(name: "", equipment: "", reps: "", time: "", sets: "", image: NSData(), details: "")
        self.stepStore.insert(stepData, atIndex: 0)
        
        self.images.insert(stepData.image, atIndex: 0)
        
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "background-2.png")!)
        
        self.view.addGestureRecognizer(self.tap)
        self.view.addGestureRecognizer(self.swipeRight)
        self.view.addGestureRecognizer(self.swipeLeft)
        self.view.addGestureRecognizer(self.swipeDown)
        
        self.detailsTextView.delegate = self
        self.equipment.delegate = self
        self.reps.delegate = self
        self.time.delegate = self
        self.sets.delegate = self
        self.stepTitle.delegate = self
        self.planTitle.delegate = self
        self.planCategory.delegate = self
        
        self.pickerView.dataSource = self
        self.pickerView.delegate = self

        self.pageControl.numberOfPages = 1
        self.pageControl.currentPage = 0
        self.setStepNumbers()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "storeInList" {
            let destination = segue.destinationViewController as! SavedPlansViewController
        
            var stepData = steps(name: "", equipment: "", reps: "", time: "", sets: "", image: NSData(), details: "")
            stepData.name = stepTitle.text
            stepData.equipment = equipment.text
            stepData.reps = reps.text
            stepData.time = time.text
            stepData.sets = sets.text
            if images.count > self.pageControl.currentPage {
                stepData.image = images[self.pageControl.currentPage]
            } else {
                stepData.image = NSData()
            }
            stepData.details = detailsTextView.text
            stepStore[self.pageControl.currentPage] = stepData
            
            var creatorID = String()
            var username:String?
            var uuid = NSUUID().UUIDString
            
            var fetchRequest = NSFetchRequest(entityName: "User")
            if let fetchResults = managedObjectContext!.executeFetchRequest(fetchRequest, error: nil) as? [User] {
                let users = fetchResults
                for user in users {
                    if user.loggedIn == true {
                        creatorID = user.id
                    }
                }
            }
            
            var programDetails = NSEntityDescription.insertNewObjectForEntityForName("Program", inManagedObjectContext: managedObjectContext!) as! Program
            programDetails.title = self.planTitle.text
            programDetails.category = self.planCategory.text
            programDetails.creatorID = creatorID
            programDetails.id = uuid
            
            for (index, item) in enumerate(stepStore) {
                var stepDetail = NSEntityDescription.insertNewObjectForEntityForName("Steps", inManagedObjectContext: managedObjectContext!) as! Steps
                stepDetail.setValue(item.name, forKey: "name")
                stepDetail.setValue(item.equipment, forKey: "equipment")
                stepDetail.setValue(item.reps, forKey: "reps")
                stepDetail.setValue(item.sets, forKey: "sets")
                stepDetail.setValue(item.time, forKey: "time")
                stepDetail.setValue(index, forKey: "number")
                stepDetail.setValue(uuid, forKey: "id")
                stepDetail.setValue(item.image, forKey: "image")
                stepDetail.setValue(item.details, forKey: "details")
            }
            
            managedObjectContext?.save(nil)
        } else if segue.identifier == "Show Image" {
            let destination = segue.destinationViewController as! ShowImageViewController
            destination.imageData = images[self.pageControl.currentPage]
        }
    }

}
