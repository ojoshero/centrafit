//
//  Steps.swift
//  CentraFit
//
//  Created by Joshua Martin on 18/02/2015.
//  Copyright (c) 2015 Joshua Martin. All rights reserved.
//

import Foundation
import CoreData

@objc(Steps)
class Steps: NSManagedObject {

    @NSManaged var details: String
    @NSManaged var equipment: String
    @NSManaged var id: String
    @NSManaged var image: NSData
    @NSManaged var name: String
    @NSManaged var number: NSNumber
    @NSManaged var reps: String
    @NSManaged var sets: String
    @NSManaged var time: String
    @NSManaged var newRelationship: User

}
