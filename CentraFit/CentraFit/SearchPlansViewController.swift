//
//  SearchPlansViewController.swift
//  CentraFit
//
//  Created by Joshua Martin on 23/02/2015.
//  Copyright (c) 2015 Joshua Martin. All rights reserved.
//

import UIKit
import CoreData

struct downloads {
    var userID:String
    var id:String
    var title:String
    var category:String
    var rating:Int
    var image:NSData
}

class SearchPlansViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {
    
    var nodes = [String]()
    var dataStore = [downloads]()
    var filtered = [downloads]()
    
    @IBOutlet weak var dataTable: UITableView!
    @IBOutlet weak var searchLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var filterSegment: UISegmentedControl!
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBAction func onFilterChanged(sender: UISegmentedControl) {
        if filterSegment.selectedSegmentIndex == 1 {
            dataStore = dataStore.sorted { $0.rating > $1.rating }
            dataTable.reloadData()
        } else if filterSegment.selectedSegmentIndex == 0 {
            //dataStore = dataStore.sorted { $0.id > $1.id }
            dataStore = filtered
            dataTable.reloadData()
        }
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
    }
    
    @IBAction func goBack(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataStore.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:SearchTableCell = tableView.dequeueReusableCellWithIdentifier("tc", forIndexPath: indexPath) as! SearchTableCell
        if (dataStore.count != 0) {
            cell.planImage?.image = UIImage(data: dataStore[indexPath.row].image)
            cell.planTitle.text = dataStore[indexPath.row].title
            cell.planCategory.text = dataStore[indexPath.row].category
            switch dataStore[indexPath.row].rating {
            case 1:
                cell.rating1.image = UIImage(named: "star_full.png")
                break
                
            case 2:
                cell.rating1.image = UIImage(named: "star_full.png")
                cell.rating2.image = UIImage(named: "star_full.png")
                break
                
            case 3:
                cell.rating1.image = UIImage(named: "star_full.png")
                cell.rating2.image = UIImage(named: "star_full.png")
                cell.rating3.image = UIImage(named: "star_full.png")
                break
                
            case 4:
                cell.rating1.image = UIImage(named: "star_full.png")
                cell.rating2.image = UIImage(named: "star_full.png")
                cell.rating3.image = UIImage(named: "star_full.png")
                cell.rating4.image = UIImage(named: "star_full.png")
                break
                
            case 5:
                cell.rating1.image = UIImage(named: "star_full.png")
                cell.rating2.image = UIImage(named: "star_full.png")
                cell.rating3.image = UIImage(named: "star_full.png")
                cell.rating4.image = UIImage(named: "star_full.png")
                cell.rating5.image = UIImage(named: "star_full.png")
                break
                
            default:
                cell.rating1.image = UIImage(named: "")
                cell.rating2.image = UIImage(named: "")
                cell.rating3.image = UIImage(named: "")
                cell.rating4.image = UIImage(named: "")
                cell.rating5.image = UIImage(named: "")
                break
            }
        }
        
        cell.planImage.layer.cornerRadius = 12.5
        cell.planImage.layer.masksToBounds = true
        cell.planImage.layer.borderColor = UIColor.whiteColor().CGColor
        cell.planImage.layer.borderWidth = 2.0
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell:SearchTableCell = tableView.dequeueReusableCellWithIdentifier("tc", forIndexPath: indexPath) as! SearchTableCell

        let row = indexPath.row
        var programDetails = NSEntityDescription.insertNewObjectForEntityForName("Program", inManagedObjectContext: managedObjectContext!) as! Program
        programDetails.title = dataStore[row].title
        programDetails.category = dataStore[row].category
        programDetails.creatorID = dataStore[row].userID
        programDetails.id = dataStore[row].id
        
        let ref = Firebase(url: "https://blinding-heat-2824.firebaseio.com/programs/" + dataStore[row].userID + "/" + dataStore[row].id + "/steps")
        ref.observeSingleEventOfType(.Value, withBlock: { snapshot in
            var children = snapshot.children
            while let child = children.nextObject() as? FDataSnapshot {
                let num = child.key.toInt()
                println(num)
                var stepDetail = NSEntityDescription.insertNewObjectForEntityForName("Steps", inManagedObjectContext: managedObjectContext!) as! Steps
                stepDetail.setValue(child.value["name"], forKey: "name")
                stepDetail.setValue(child.value["equipment"], forKey: "equipment")
                stepDetail.setValue(child.value["reps"], forKey: "reps")
                stepDetail.setValue(child.value["sets:"], forKey: "sets")
                stepDetail.setValue(child.value["time"], forKey: "time")
                stepDetail.setValue(num, forKey: "number")
                var imgString = child.value["image"] as! String
                var imgNS = NSData(base64EncodedString: imgString, options: NSDataBase64DecodingOptions())
                stepDetail.setValue(imgNS!, forKey: "image")
                stepDetail.setValue(self.dataStore[row].id, forKey: "id")
                stepDetail.setValue(child.value["details"], forKey: "details")
            }
            
            managedObjectContext!.save(nil)
            cell.accessoryType = UITableViewCellAccessoryType.Checkmark
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.dataTable.delegate = self
        self.dataTable.dataSource = self
        self.searchBar.delegate = self
        self.dataTable.reloadData()

        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "background-2.png")!)
        self.activityIndicator.hidden = false
        self.activityIndicator.startAnimating()
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        
        dispatch_async(dispatch_get_main_queue(), {
            let ref2 = Firebase(url: "https://blinding-heat-2824.firebaseio.com/programs")
            ref2.observeSingleEventOfType(.Value, withBlock: { snapshot in
                var children = snapshot.children
                while let child = children.nextObject() as? FDataSnapshot {
                    self.nodes.append(child.key)
                    var key = child.key
                    dispatch_async(dispatch_get_main_queue(), {
                        var ref3 = Firebase(url: "https://blinding-heat-2824.firebaseio.com/programs/" + key)
                        ref3.observeSingleEventOfType(.Value, withBlock: { snapshot in
                            var children = snapshot.children
                            while let child = children.nextObject() as? FDataSnapshot {
                                var theData = downloads(userID: "", id: "", title: "", category: "", rating: 0, image: NSData())
                                theData.userID = key
                                theData.id = child.key
                                theData.title = child.value["title"] as! String
                                theData.category = child.value["category"] as! String
                                theData.rating = child.value["rating"] as! Int
                                dispatch_async(dispatch_get_main_queue(), {
                                    var ref4 = Firebase(url: "https://blinding-heat-2824.firebaseio.com/programs/" + key + "/" + child.key + "/" + "steps/" + "0")
                                    ref4.observeSingleEventOfType(.Value, withBlock: { snapshot in
                                        var imgString = snapshot.value["image"] as! String
                                        var imgNS = NSData(base64EncodedString: imgString, options: NSDataBase64DecodingOptions())
                                        theData.image = imgNS!
                                        self.dataStore.append(theData)
                                        self.filtered = self.dataStore
                                        self.dataTable.reloadData()
                                        self.activityIndicator.stopAnimating()
                                        self.activityIndicator.hidden = true
                                        self.searchLabel.hidden = true
                                    })
                                })
                            }
                        })
                    })
                }
            })
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
        })

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
