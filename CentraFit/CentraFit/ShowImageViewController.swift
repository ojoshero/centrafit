//
//  ShowImageViewController.swift
//  CentraFit
//
//  Created by Joshua Martin on 17/02/2015.
//  Copyright (c) 2015 Joshua Martin. All rights reserved.
//

import UIKit

class ShowImageViewController: UIViewController {

    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var largeImage: UIImageView!
    
    var imageData = NSData()
    
    @IBAction func closePage(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "background-2.png")!)
        self.view.opaque = false
        
        self.largeImage.image = UIImage(data: imageData)
        
        
        self.largeImage.layer.cornerRadius = 12.5
        self.largeImage.layer.masksToBounds = true
        self.largeImage.layer.borderColor = UIColor.whiteColor().CGColor
        self.largeImage.layer.borderWidth = 2.0
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
