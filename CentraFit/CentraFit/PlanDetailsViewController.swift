//
//  PlanDetailsViewController.swift
//  CentraFit
//
//  Created by Joshua Martin on 20/02/2015.
//  Copyright (c) 2015 Joshua Martin. All rights reserved.
//

import UIKit

struct programStruct {
    var category: String
    var creatorID: String
    var id: String
    var title: String
    var rating: Int
    var downloads: Int
}

class PlanDetailsViewController: UIViewController {
    
    var stepStore = [displaySteps]()
    var items = [programStruct]()

    @IBOutlet weak var image1: UIImageView!
    @IBOutlet weak var image2: UIImageView!
    @IBOutlet weak var image3: UIImageView!
    @IBOutlet weak var image4: UIImageView!
    @IBOutlet weak var image5: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var downloadedLabel: UILabel!
    @IBOutlet weak var planTitle: UILabel!
    @IBOutlet weak var uploadButton: UIButton!
    
    var nameText:String?
    var planTitleText:String?
    var buttonHidden:Bool?
    var rating:Int?
    
    @IBAction func goBack(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func UploadPlan(sender: UIButton) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        dispatch_async(dispatch_get_main_queue(), {
            let newProgram = [
                "title": self.items[0].title,
                "category": self.items[0].category,
                "downloads": self.items[0].downloads,
                "rating": self.items[0].rating
            ]
            ref.childByAppendingPath("programs").childByAppendingPath(self.items[0].creatorID).childByAppendingPath(self.items[0].id).setValue(newProgram)
            for step in self.stepStore {
                var imageString = step.image.base64EncodedStringWithOptions(.allZeros)
                let newStep = [
                    "name": step.name,
                    "equipment": step.equipment,
                    "reps": step.reps,
                    "time": step.time,
                    "sets:": step.sets,
                    "image": imageString,
                    "details": step.details
                ]
                ref.childByAppendingPath("programs").childByAppendingPath(self.items[0].creatorID).childByAppendingPath(self.items[0].id).childByAppendingPath("steps").childByAppendingPath(step.number.stringValue).setValue(newStep)
                UIApplication.sharedApplication().networkActivityIndicatorVisible = true
            }
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let nT = self.nameText {
            self.nameLabel.text = nT
            if let pTT = self.planTitleText {
                self.planTitle.text = pTT
                if let bH = self.buttonHidden {
                    self.uploadButton.hidden = bH
                }
            }
        }
        
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "background-2.png")!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
