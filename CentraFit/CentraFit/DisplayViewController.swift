//
//  DisplayViewController.swift
//  CentraFit
//
//  Created by Joshua Martin on 06/02/2015.
//  Copyright (c) 2015 Joshua Martin. All rights reserved.
//

import UIKit

struct displaySteps {
    var name:String
    var equipment:String
    var reps:String
    var time:String
    var sets:String
    var image:NSData
    var number:NSNumber
    var details:String
}

class DisplayViewController: UIViewController, UIGestureRecognizerDelegate {
    
    var stepStore = [displaySteps]()
    var orderedStepStore = [displaySteps]()
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var planTitle: UILabel!
    @IBOutlet weak var category: UILabel!
    @IBOutlet weak var stepView: UIScrollView!
    @IBOutlet weak var stepLabel: UILabel!
    @IBOutlet weak var stepImage: UIImageView!
    @IBOutlet weak var stepName: UILabel!
    @IBOutlet weak var stepEquipment: UILabel!
    @IBOutlet weak var stepReps: UILabel!
    @IBOutlet weak var stepTime: UILabel!
    @IBOutlet weak var stepSets: UILabel!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet var leftSwipe: UISwipeGestureRecognizer!
    @IBOutlet var rightSwipe: UISwipeGestureRecognizer!
    @IBOutlet weak var detailsTextView: UITextView!
    
    var planTitleText:String?
    var categoryText:String?
    
    @IBAction func goBack(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func swipeLeft(sender: UISwipeGestureRecognizer) {
        if (self.pageControl.currentPage + 1 != self.pageControl.numberOfPages) {
            
            let transitionOptions = UIViewAnimationOptions.TransitionFlipFromLeft
            
            UIView.transitionWithView(self.stepView, duration: 0.8, options: transitionOptions, animations: {
                
                }, completion: { finished in
                    self.pageControl.currentPage++
                    self.setLabels()
            })
        }

    }
    
    @IBAction func swipeRight(sender: UISwipeGestureRecognizer) {
        if (self.pageControl.currentPage != 0) {
            
            let transitionOptions = UIViewAnimationOptions.TransitionFlipFromRight
            
            UIView.transitionWithView(self.stepView, duration: 0.8, options: transitionOptions, animations: {
                
                }, completion: { finished in
                    self.pageControl.currentPage--
                    self.setLabels()
            })
        }
    }
    
    func setLabels() {
        self.stepName.text = self.orderedStepStore[self.pageControl.currentPage].name
        self.stepEquipment.text = self.orderedStepStore[self.pageControl.currentPage].equipment + " required"
        self.stepReps.text = self.orderedStepStore[self.pageControl.currentPage].reps + " reps"
        self.stepSets.text = self.orderedStepStore[self.pageControl.currentPage].sets + " sets"
        self.stepTime.text = self.orderedStepStore[self.pageControl.currentPage].time
        self.stepLabel.text = "Step " + String(self.pageControl.currentPage + 1) + " of " + String(self.pageControl.numberOfPages)
        self.stepImage.image = UIImage(data: self.orderedStepStore[self.pageControl.currentPage].image)
        self.detailsTextView.text = self.orderedStepStore[self.pageControl.currentPage].details
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let pt:String = planTitleText {
            self.planTitle.text = pt
            if let ct:String = categoryText {
                self.category.text = ct
            }
        }
        
        var tempstep = displaySteps(name: "", equipment: "", reps: "", time: "", sets: "", image: NSData(), number: NSNumber(), details: "")
        for i in stepStore {
            orderedStepStore.append(tempstep)
        }
        for item in stepStore {
            orderedStepStore[Int(item.number)] = item
        }
        
        self.view.addGestureRecognizer(self.rightSwipe)
        self.view.addGestureRecognizer(self.leftSwipe)
        
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "background-2.png")!)
        
        self.pageControl.numberOfPages = self.stepStore.count
        self.pageControl.currentPage = 0
        self.setLabels()
        
        stepImage.layer.cornerRadius = 12.5
        stepImage.layer.masksToBounds = true
        stepImage.layer.borderColor = UIColor.whiteColor().CGColor
        stepImage.layer.borderWidth = 2.0

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
