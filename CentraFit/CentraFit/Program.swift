//
//  Program.swift
//  CentraFit
//
//  Created by Joshua Martin on 19/02/2015.
//  Copyright (c) 2015 Joshua Martin. All rights reserved.
//

import Foundation
import CoreData

@objc(Program)
class Program: NSManagedObject {

    @NSManaged var category: String
    @NSManaged var id: String
    @NSManaged var creatorID: String
    @NSManaged var title: String
    @NSManaged var rating: NSNumber
    @NSManaged var downloads: NSNumber
    @NSManaged var newRelationship: User

}
